FROM node:22-alpine3.18 as builder

WORKDIR /app
COPY . ./
RUN npm update && npm install

FROM node:22-alpine3.18
WORKDIR /app/ 
COPY --from=builder /app ./
CMD ["npm", "run-script","dev"]